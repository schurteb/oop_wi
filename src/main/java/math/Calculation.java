package math;

public class Calculation {
    /**
     * Main entry point
     *
     * @param args Program arguments
     */
    public static void main(String[] args) {

    }

    /**
     * Addition operation
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of a + b
     */
    public static double add(double a, double b) {
        return a + b;
    }

    /**
     * Subtraction operation
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of a - b
     */
    public static double sub(double a, double b) {
        return a - b;
    }

    /**
     * Multiplication operation
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of a * b
     */
    public static double mul(double a, double b) {
        return a * b;
    }

    /**
     * Division operation
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of a / b
     */
    public static double div(double a, double b) {
        return a / b;
    }

    /**
     * Modulo operation
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of a % b
     */
    public static double mod(double a, double b) {
        return a % b;
    }
}
