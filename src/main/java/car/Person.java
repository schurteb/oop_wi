package car;

public class Person {
    private String name;

    private String firstName;

    /**
     * Initializes a new person
     *
     * @param firstName The first name of the person
     * @param name The name of the person
     */
    public Person(String firstName, String name) {
        this.setFirstName(firstName);
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
