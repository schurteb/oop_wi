package car;

import java.util.ArrayList;

public class Car {
    /**
     * List of persons in the car
     */
    private ArrayList<Person> persons;

    /**
     * Creates a new car
     */
    public Car() {
        this.persons = new ArrayList<Person>();
    }

    /**
     * Add a person to the car
     *
     * @param p The person to add
     */
    public void einsteigen(Person p) {
        this.persons.add(p);
    }

    /**
     * Kick a person out of the car
     *
     * @param p The person to kick out
     */
    public void aussteigen(Person p) {
        this.persons.removeIf(person -> person.equals(p));
    }

    /**
     * Get persons remaining in the car
     *
     * @return Returns ArrayList List of persons remaining in the car
     */
    public ArrayList<Person> remainingPersons() {
        return this.persons;
    }
}
