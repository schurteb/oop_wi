package car;

public class Main {
    /**
     * Main entry point
     *
     * @param args Program arguments
     */
    public static void main(String[] args) {
        Person p1 = new Person("Peter", "Meier");
        Person p2 = new Person("Hans", "Schmid");
        Person p3 = new Person("Roger", "Müller");

        Car c = new Car();

        c.einsteigen(p1);

        printRemainingPersonsNicely("Part 1", c);

        c.einsteigen(p2);
        c.einsteigen(p3);

        printRemainingPersonsNicely("Part 2", c);

        c.aussteigen(p3);

        printRemainingPersonsNicely("Part 3", c);
    }

    /**
     * Prints the list from Car.remainingPersons() nicely to the console
     *
     * @param section String Title printed before the list entries
     * @param c Car The car to query the list of persons
     */
    public static void printRemainingPersonsNicely(String section, Car c) {
        if (c.remainingPersons().size() > 0) {
            System.out.printf("\n");
            System.out.printf("%s - %d Persons\n", section, c.remainingPersons().size());

            for (Person remainingPerson : c.remainingPersons()) {
                System.out.printf("%s %s\n", remainingPerson.getFirstName(), remainingPerson.getName());
            }
        }
    }

    /**
     * Does something with strings
     * @param params String[] Some input arguments
     * @return Strings[] Returns some string array
     */
    public static String[] randomFunctionWithoutComment(String[] params) {
        return params;
    }
}
