package hello;

import org.joda.time.LocalTime;

public class HelloWorld {
    /**
     * Main entry point
     *
     * @param args Program arguments
     */
    public static void main(String[] args) {
        // LocalTime currentTime = LocalTime.now();
        LocalTime currentTime = new LocalTime();
        System.out.println("The current local time is: " + currentTime);

        Greeter greeter = new Greeter();
        System.out.println(greeter.sayHello());
    }
}
