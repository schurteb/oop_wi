package car;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Test;

public class CarTests {
    private ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private PrintStream ps = new PrintStream(baos);

    @Before
    public void setup() {
        System.setOut(ps);
    }

    /**
     * Checks that part 1 and it's person amount is printed to console
     */
    @Test
    public void shouldPrintPart1ToConsole() {
        Main.main(new String[] {});

        System.out.println("TestPart1");

        assertThat(output(), containsString("Part 1 - 1 Persons"));
    }

    /**
     * Checks that part 2 and it's person amount is printed to console
     */
    @Test
    public void shouldPrintPart2ToConsole() {
        Main.main(new String[] {});

        assertThat(output(), containsString("Part 2 - 3 Persons"));
    }

    /**
     * Checks that part 3 and it's person amount is printed to console
     */
    @Test
    public void shouldPrintPart3ToConsole() {
        Main.main(new String[] {});

        assertThat(output(), containsString("Part 3 - 2 Persons"));
    }

    private String output() {
        return new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }
}
