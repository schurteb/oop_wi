package math;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class CalculationTests {
    private ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private PrintStream ps = new PrintStream(baos);

    @Before
    public void setup() {
        System.setOut(ps);
    }

    /**
     * Whether time should be printed to console
     */
    @Test
    public void shouldAddCorrect() {
        assertThat(Calculation.add(1.0, 2.0), is(3.0));
    }

    /**
     * Whether time should be printed to console
     */
    @Test
    public void shouldSubtractCorrect() {
        assertThat(Calculation.sub(5.0, 2.0), is(3.0));
    }

    /**
     * Whether time should be printed to console
     */
    @Test
    public void shouldMultiplyCorrect() {
        assertThat(Calculation.mul(3.0, 15.0), is(45.0));
    }

    /**
     * Whether time should be printed to console
     */
    @Test
    public void shouldDivideCorrect() {
        assertThat(Calculation.div(9.0, 3.0), is(3.0));
    }

    /**
     * Whether time should be printed to console
     */
    @Test
    public void shouldModuloCorrect() {
        assertThat(Calculation.mod(10.0, 3.0), is(1.0));
    }
}
